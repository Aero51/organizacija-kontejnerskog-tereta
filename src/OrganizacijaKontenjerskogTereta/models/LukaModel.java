package OrganizacijaKontenjerskogTereta.models;

import java.io.Serializable;


public class LukaModel extends AbstractModelObject implements Serializable{
	private String luke="luka";
	
	
	public String getLuke() {
		return luke;
	}
	public void setLuke(String luke) {
		//this.luke = luke;
		
		firePropertyChange("luke", this.luke, this.luke = luke);
	}
	

}
