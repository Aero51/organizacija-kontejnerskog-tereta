package OrganizacijaKontenjerskogTereta.models;

import java.io.Serializable;

public class IsporukaModel extends AbstractModelObject implements Serializable{

	private String polaznaLuka;
	private String odrediste;
	private String materijal;
	private String kolicina;
	public String getOdrediste() {
		return odrediste;
	}
	public void setOdrediste(String odrediste) {
		this.odrediste = odrediste;
		//firePropertyChange("odrediste", this.odrediste, this.odrediste = odrediste);
	}
	public String getPolaznaLuka() {
		return polaznaLuka;
	}
	public void setPolaznaLuka(String polaznaLuka) {
		this.polaznaLuka = polaznaLuka;
		//firePropertyChange("polaznaLuka", this.polaznaLuka, this.polaznaLuka = polaznaLuka);
	}
	public String getMaterijal() {
		return materijal;
	}
	public void setMaterijal(String materijal) {
		this.materijal = materijal;
		//firePropertyChange("materijal", this.materijal, this.materijal = materijal);
	}
	public String getKolicina() {
		return kolicina;
		
	}
	public void setKolicina(String kolicina) {
		//this.kolicina = kolicina;
		firePropertyChange("kolicina", this.kolicina, this.kolicina = kolicina);
	}
	
	
}
