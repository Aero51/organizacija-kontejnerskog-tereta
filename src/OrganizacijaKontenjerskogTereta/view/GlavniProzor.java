package OrganizacijaKontenjerskogTereta.view;

import OrganizacijaKontenjerskogTereta.controllers.LukeKontroler;
import OrganizacijaKontenjerskogTereta.controllers.PostavkeCarobnjakaKontroler;
import OrganizacijaKontenjerskogTereta.models.LukaModel;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;



public class GlavniProzor {
	private DataBindingContext m_bindingContext;

	protected Shell shellGlavniProzor;
	private LukeKontroler narudzbenicaKontroler= new LukeKontroler();
	private Text txtUbaciLuku;
	private WritableList listaluka;
	private ListViewer listViewer;
	//private PrijavauSustavDialog prijavaDialog ; // final da bi radio unutar klase button unos luke
	private boolean izlaz;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Realm.runWithDefault(SWTObservables.getRealm(display), new Runnable() {
			public void run() {
				try {
					GlavniProzor window = new GlavniProzor();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		 PrijavauSustavDialog	prijavaDialog=new PrijavauSustavDialog(shellGlavniProzor, SWT.DIALOG_TRIM);
		String naslov = "Dobrodošli";
		prijavaDialog.open(naslov);
		izlaz=prijavaDialog.isIzlazIzPrograma();
		//System.out.println(izlaz);
		if(izlaz!=true)
		{
			
		shellGlavniProzor.open();
		shellGlavniProzor.layout();
		while (!shellGlavniProzor.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		}
		else 
		{//shellGlavniProzor.dispose();
		shellGlavniProzor.close();}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shellGlavniProzor = new Shell();
		shellGlavniProzor.setImage(SWTResourceManager.getImage(GlavniProzor.class, "/Slike/images (4).jpg"));
		
	
		shellGlavniProzor.setSize(350, 266);
		
		Button btnPregledIsporuka = new Button(shellGlavniProzor, SWT.NONE);
		btnPregledIsporuka.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				PregledIsporukaDialog pregledIsporukaDialog=new PregledIsporukaDialog(shellGlavniProzor, SWT.DIALOG_TRIM);
				
				pregledIsporukaDialog.open();
				
				
			}
		});
		btnPregledIsporuka.setBounds(32, 22, 105, 23);
		btnPregledIsporuka.setText("Pregled isporuka");
		
		Button btnNovaNarudzbenica = new Button(shellGlavniProzor, SWT.NONE);
		btnNovaNarudzbenica.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
			//NovaNarudzbenica prozor= new NovaNarudzbenica(shellGlavniProzor, SWT.DIALOG_TRIM);
			//prozor.open();
			
			//Shell shell;
			//shell = new Shell();
			//shell.setSize(450, 300);
			//shell.setText("SWT Application");
			PostavkeCarobnjakaKontroler wizard = new PostavkeCarobnjakaKontroler();
			WizardDialog carobnjakDialog = new WizardDialog(shellGlavniProzor, wizard);
			carobnjakDialog.open();
			
			}
		});
		btnNovaNarudzbenica.setBounds(32, 61, 105, 23);
		btnNovaNarudzbenica.setText("Nova Narudžbenica");
		
		final Composite composite = new Composite(shellGlavniProzor, SWT.NONE);
		composite.setBounds(10, 164, 226, 68);
		composite.setVisible(false);
		
		//final PrijavauSustavDialog dialog = new PrijavauSustavDialog(shellGlavniProzor, SWT.DIALOG_TRIM); // final da bi radio unutar klase button unos luke
		//boolean izlaz;
		
		 Button btnUnosLuke = new Button(shellGlavniProzor, SWT.NONE);
		btnUnosLuke.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				String naslov="Unesite lozinku";  //potrebno promijeniti u kontroleru
				PrijavauSustavDialog unosLukePrijavaDialog=new PrijavauSustavDialog(shellGlavniProzor, SWT.DIALOG_TRIM);
				unosLukePrijavaDialog.open(naslov);
				izlaz=unosLukePrijavaDialog.isIzlazIzPrograma();
				//System.out.println("Izlaz ,povratak u glavni"+izlaz);
				if(izlaz==false)
				{
				composite.setVisible(true);
				}
			}
		});
		btnUnosLuke.setBounds(32, 101, 68, 23);
		btnUnosLuke.setText("Unos luke");
		
		
		
		Button btnUbaciLuku = new Button(composite, SWT.NONE);
		btnUbaciLuku.setBounds(59, 23, 68, 23);
		btnUbaciLuku.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				
				
				
				
				try {
					String ubacuLuku;
					ubacuLuku=txtUbaciLuku.getText();
					//System.out.println(ubacuLuku);
					narudzbenicaKontroler.spremiLuku(ubacuLuku);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnUbaciLuku.setText("Ubaci luku");
		
		txtUbaciLuku = new Text(composite, SWT.BORDER);
		txtUbaciLuku.setBounds(56, 1, 76, 19);
		
		Label lblUpisiLuku = new Label(composite, SWT.NONE);
		lblUpisiLuku.setBounds(0, 4, 49, 13);
		lblUpisiLuku.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblUpisiLuku.setText("Upiši luku:");
		
		listViewer = new ListViewer(composite, SWT.BORDER | SWT.V_SCROLL);
		org.eclipse.swt.widgets.List list = listViewer.getList();
		list.setBounds(138, 0, 88, 68);
		
		Button btnZatvori = new Button(composite, SWT.NONE);
		btnZatvori.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				composite.setVisible(false);
			}
		});
		btnZatvori.setBounds(59, 45, 57, 23);
		btnZatvori.setText("Zatvori");
		
		Label label = new Label(composite, SWT.NONE);
		label.setImage(SWTResourceManager.getImage(GlavniProzor.class, "/Slike/preuzmi.jpg"));
		label.setBounds(0, 14, 53, 54);
		
		
		Label lblSlika = new Label(shellGlavniProzor, SWT.NONE);
		lblSlika.setImage(SWTResourceManager.getImage(GlavniProzor.class, "/Slike/containership.jpg"));
		lblSlika.setBounds(0, 0, 342, 232);
		
		
		//System.out.println(lblSLika.getSize());
		//
		String naslov = "Dobrodošli";
		//dialog.open(naslov);
		//izlaz=dialog.isIzlazIzPrograma();
		//System.out.println(izlaz);
		
		
		
	//izlaz=dialog.isIzlazIzPrograma();
		//	if(izlaz==true)
			{
				//shellGlavniProzor.dispose();
			//	shellGlavniProzor.close();
			}
				
		//System.out.println(izlaz);
		listaluka=new WritableList(narudzbenicaKontroler.ucitajLuke(),LukeKontroler.class);
		//System.out.println(listaluka.get(0));
		//System.out.println(listaluka.size());
		m_bindingContext = initDataBindings();
		
		
	
	}
	
	//private void ubaciLuku(){
		//LukaModel spremnikModel = new LukaModel();  //stvara novi objekt 
	//}
	/*	listaLuka.add(trenutniIndeks, osobeModel);
		
		txtPrezime.setText("");  
		*/
	
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
		listViewer.setContentProvider(listContentProvider);
		//
		IObservableMap observeMap = BeansObservables.observeMap(listContentProvider.getKnownElements(), LukaModel.class, "luke");
		listViewer.setLabelProvider(new ObservableMapLabelProvider(observeMap));
		//
		listViewer.setInput(listaluka);
		
		return bindingContext;
	}
}
