package OrganizacijaKontenjerskogTereta.view;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import OrganizacijaKontenjerskogTereta.controllers.PrijavaUSustavKontroler;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Image;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.wb.swt.SWTResourceManager;


public class PrijavauSustavDialog extends Dialog {

	protected Object result;
	protected Shell shlPrijavauSustav;
	private Text txtKorisnik;
	private Text txtLozinka;
	private PrijavaUSustavKontroler kontroler = new PrijavaUSustavKontroler();
//	PrijavaUsustavModel prijavaUsustavModel = new PrijavaUsustavModel();
    private int brojPokusaja;
    private Text txtPreostaloPoku;
    private boolean izlazIzPrograma;
    
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public PrijavauSustavDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @param naslov 
	 * @return the result
	 */
	public Object open(String naslov) {
		createContents();
		shlPrijavauSustav.open();
		shlPrijavauSustav.layout();
		Display display = getParent().getDisplay();
		while (!shlPrijavauSustav.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlPrijavauSustav = new Shell(getParent(), SWT.DIALOG_TRIM);
		shlPrijavauSustav.addShellListener(new ShellAdapter() {
			
			
			@Override
			public void shellClosed(ShellEvent dogadjaj) {
				//System.out.println(kontroler.isStanje());
				if(kontroler.isStanje()==true)
				{izlazIzPrograma=false;}
				else{
				MessageBox izlaz = new MessageBox (shlPrijavauSustav, SWT.ICON_QUESTION |SWT.OK |SWT.CANCEL);
				izlaz.setText("Potvrda izlaza");
				izlaz.setMessage("Jeste li sigurni da zelite izaći?");
				int potvrda = izlaz.open();
				dogadjaj.doit=potvrda == SWT.OK;
				//izlazIzPrograma
				
				//System.out.println(potvrda);
				
				//if(potvrda==32)
				{
					izlazIzPrograma=true;
				//shlPrijavauSustav.close();
				//shlPrijavauSustav.dispose();
				}
				}
			}
		});
		
		shlPrijavauSustav.setSize(287, 139);
		shlPrijavauSustav.setText("Unesite lozinku");
		shlPrijavauSustav.setLocation(350,300);
		
		txtLozinka = new Text(shlPrijavauSustav, SWT.BORDER);
		txtLozinka.setBounds(91, 38, 76, 19);
		txtLozinka.setEchoChar('*');
		txtLozinka.setTextLimit(10);
		
		
		txtKorisnik = new Text(shlPrijavauSustav, SWT.BORDER | SWT.READ_ONLY);
		txtKorisnik.setText("Korisnik");
		txtKorisnik.setEditable(false);
		txtKorisnik.setBounds(91, 13, 76, 19);
		
		
		Label lblKorisnickoIme = new Label(shlPrijavauSustav, SWT.NONE);
		lblKorisnickoIme.setBounds(10, 16, 75, 13);
		lblKorisnickoIme.setText("Tip korisnika:");
		
		Label lblLozinka = new Label(shlPrijavauSustav, SWT.NONE);
		lblLozinka.setBounds(10, 43, 75, 13);
		lblLozinka.setText("Lozinka:");
		
		  
		
		
		
		txtPreostaloPoku = new Text(shlPrijavauSustav, SWT.BORDER | SWT.READ_ONLY);
		txtPreostaloPoku.setVisible(false);
		
		
		
		txtPreostaloPoku.setBounds(179, 13, 76, 19);
		
	//	txtLozinka.set
	
		  
		
		Button btnPrihvati = new Button(shlPrijavauSustav, SWT.NONE);
		btnPrihvati.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				String sifra,sifraAdmina = null;
				//sifraAdmina="nema"; 
				sifra=String.valueOf(txtLozinka.getText());
				//System.out.println(sifra);
				brojPokusaja++;
				txtPreostaloPoku.setVisible(true);
				txtPreostaloPoku.setText("Poku\u0161aj "+brojPokusaja+"/5");
				
				//kontroler.setSifra();
				//System.out.println(brojPokusaja);
				try {
					kontroler.provjeraSifre(sifra);
					
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					MessageBox poruka = new MessageBox(shlPrijavauSustav);   
					poruka.setMessage(e.getMessage());   
					poruka.setText("Greška!");        
					poruka.open();
					
					//System.out.println(prijavaUsustavModel.isStatus());
					//System.out.println(prijavaUsustavModel.getStanje());
					/*if(prijavaUsustavModel.isStatus()==true)
					{shlPrijavauSustav.close();}
					*/

					if (brojPokusaja>4)
					{
						txtKorisnik.setText("Admin");
						txtPreostaloPoku.setVisible(false);
					}
					
					if(kontroler.isStanje()==true)
					{
						//System.out.println(kontroler.isStanje());
						shlPrijavauSustav.close();
					}
				}
				
			}
		});
		btnPrihvati.setBounds(91, 74, 68, 23);
		btnPrihvati.setText("Prihvati");
		
		Label lblSlika = new Label(shlPrijavauSustav, SWT.NONE);
		lblSlika.setImage(SWTResourceManager.getImage(PrijavauSustavDialog.class, "/Slike/password.jpg"));
		
		lblSlika.setBounds(187, 38, 68, 66);
		
		//System.out.println(lblSlika.getSize());
		
		//labelSlika.setImage(arg0)
		

	}

	public boolean isIzlazIzPrograma() {
		return izlazIzPrograma;
	}

	public void setIzlazIzPrograma(boolean izlazIzPrograma) {
		this.izlazIzPrograma = izlazIzPrograma;
	}
}
 //  nedostaje kontrola  ako se x pretisne da zatvori prozor