package OrganizacijaKontenjerskogTereta.view;


import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.widgets.List;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.TableViewer;

import OrganizacijaKontenjerskogTereta.controllers.LukeKontroler;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.beans.BeansObservables;
import OrganizacijaKontenjerskogTereta.models.LukaModel;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.dialogs.IDialogConstants;

public class NovaNarudzbenicaDialog extends WizardPage {
	private DataBindingContext m_bindingContext;
	 String labela;
	 Text txtUnos;
	private  String rezultat;
	private String kolicina;
	private WritableList listaluka;
	 List list ;
	private int brojac=0;

	 private ListViewer listViewer;
	 
	 private Label lblKolicina;
	 private Text txtKolicina;
	 
    public NovaNarudzbenicaDialog(String txt,String opis) {
        super("Carobnjak stranica");
        setTitle("Nova narudžbenica");
        setDescription(opis);
        labela=txt;
        setPageComplete(false);
    }

    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        setControl(container);
        
        container.setLayout(new GridLayout(2, false));
//container.
        Label lblLabela = new Label(container, SWT.NONE);
        lblLabela.setText(labela);
         
      //  getButton(IDialogConstants.FINISH_ID);

          txtUnos = new Text(container, SWT.BORDER);
          GridData gd_txtUnos = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
          gd_txtUnos.widthHint = 189;
          txtUnos.setLayoutData(gd_txtUnos);
          new Label(container, SWT.NONE);
          listViewer = new ListViewer(container, SWT.BORDER | SWT.V_SCROLL);
          list = listViewer.getList();
          list.setEnabled(false);
          list.setVisible(false);
          
          
         if(labela.equals("Unesite vrstu tereta:"))      // ovo samo u slucaju stranice 3 se pali
          { 
          lblKolicina = new Label(container, SWT.NONE);
          lblKolicina.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
          lblKolicina.setText("Unesite količinu :");
          txtKolicina = new Text(container, SWT.BORDER);
          txtKolicina.addModifyListener(new ModifyListener() {
          	public void modifyText(ModifyEvent arg0) {
          			if (txtUnos.getText().length() != 0&& txtKolicina.getText().length() !=0)
          			{
          				kolicina=String.valueOf(txtKolicina.getText());
          				rezultat=String.valueOf(txtUnos.getText());
          				setPageComplete(true);
          			}  
          			else
          			{
          				setPageComplete(false);	
          			}
          	}
          });
          GridData gd_txtKolicina = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
          gd_txtKolicina.widthHint = 195;
          txtKolicina.setLayoutData(gd_txtKolicina);
          
          }
          LukeKontroler narudzbenicaKontroler=new LukeKontroler();
          listaluka=new WritableList(narudzbenicaKontroler.ucitajLuke(),LukeKontroler.class);
         // System.out.println("Velicina liste je:"+listaluka.size());
          if(labela.equals("Unesite vrstu tereta:"))
          {}
          else
          {    
          txtUnos.addModifyListener(new ModifyListener() 
          {
          	public void modifyText(ModifyEvent arg0) 
          	{
          	 
          		String stringUnos = txtUnos.getText().toLowerCase();
          		boolean provjera=false;
    			if (stringUnos.length() == 0) {
    				  list.setEnabled(false);
    		          list.setVisible(false);
    			} else {
    				   list.setEnabled(true);
    				   list.setVisible(true);
    				  // System.out.println(list.getItemCount());
    				 //  System.out.println("Velicina liste je:"+listaluka.size());
    				  // String provjera = "";
    				   		for( int i=0; i< listaluka.size();i++)
    				   		{
    				   			String elementListe=list.getItem(i).toLowerCase();
    				    
    				   				if(elementListe.startsWith(stringUnos))
    				   				{
    				  // provjera= elementListe.contains(stringUnos);
    				   					list.select(i);
    				   //txtUnos.setText(list.getItem(i));
    				   //txtUnos.setSelection(stringUnos.length(), elementListe.length());
    				   					if(txtUnos.getText().equals(list.getItem(i)))
    				   					{
    			        	//System.out.println(listaluka.get(brojac));
    				   						rezultat=txtUnos.getText();
    				   						setPageComplete(true);
    				   					}
    				   					else{
    				   						setPageComplete(false);
    				   						}
    				   				}
    				   		}
    				   System.out.println(provjera);
    				   System.out.println(brojac);
    					}
    		}
          });
          
          txtUnos.addListener(SWT.KeyDown, new Listener() 
          {
      		public void handleEvent(Event event) 
      		{
      			switch (event.keyCode) 
      			{
      				case SWT.ARROW_DOWN:
      					int index = (list.getSelectionIndex() + 1) % list.getItemCount();
      					System.out.println("case SWT.ARROW_DOWN: Ovo je selection index "+list.getSelectionIndex());
      					list.setSelection(index);
      					event.doit = false;
      					break;
      				case SWT.ARROW_UP:
      					index = list.getSelectionIndex() - 1;
      					if (index < 0) index = list.getItemCount() - 1;
      					list.setSelection(index);
      					System.out.println("case SWT.ARROW_UP: Ovo je selection index "+list.getSelectionIndex());
      					event.doit = false;
      					break;
      				case SWT.CR:
      					if( list.getSelectionIndex() != -1)
      					{
      						txtUnos.setText(list.getSelection()[0]);
      						System.out.println("case SWT.CR: Ovo je selection index "+list.getSelectionIndex());
      						list.setEnabled(false);
      						list.setVisible(false);	
      					}
      					break;
      				case SWT.ESC:   // ne radi jer gasi cijeli prozor na esc
      					list.setEnabled(false);
        		        list.setVisible(false);
      					break;	
      			}
      		}
      	});
          
          list.addListener(SWT.DefaultSelection, new Listener() {
      		public void handleEvent(Event event) {
      			txtUnos.setText(list.getSelection()[0]);
      			System.out.println("SWT.DefaultSelection list.getSelection()[0] "+list.getSelection()[0]);
      			list.setEnabled(false);
		        list.setVisible(false);
      		}
      	});
      	list.addListener(SWT.KeyDown, new Listener() {
      		public void handleEvent(Event event) {
      			if (event.keyCode == SWT.ESC) {
      				System.out.println("SWT.KeyDown Ovo je event.keycode "+event.keyCode);
      				list.setEnabled(false);
    		        list.setVisible(false);
      			}
      		}
      	}); 
          
         m_bindingContext = initDataBindings();  
    }
    }
    
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
		listViewer.setContentProvider(listContentProvider);
		//
		IObservableMap observeMap = BeansObservables.observeMap(listContentProvider.getKnownElements(), LukaModel.class, "luke");
		listViewer.setLabelProvider(new ObservableMapLabelProvider(observeMap));
		//
		listViewer.setInput(listaluka);
		//
		return bindingContext;
	}

	public String getRezultat() {
		return rezultat;
	}

	public void setRezultat(String rezultat) {
		this.rezultat = rezultat;
		
	}

	public String getKolicina() {
		return kolicina;
	}

	public void setKolicina(String kolicina) {
		this.kolicina = kolicina;
	}
}