package OrganizacijaKontenjerskogTereta.view;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.widgets.List;

import OrganizacijaKontenjerskogTereta.controllers.PostavkeCarobnjakaKontroler;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.beans.BeansObservables;
import OrganizacijaKontenjerskogTereta.models.IsporukaModel;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class PregledIsporukaDialog extends Dialog {
	private DataBindingContext m_bindingContext;

	protected Object result;
	protected Shell shellPregledIsporuka;
	private Table table;
	private WritableList listaNarudzbi;
	private TableViewer tableViewer;
	TableColumn tblclmnPolaznaLuka;
	TableColumn tblclmnMaterijal;
	TableColumn tblclmnOdrediste;
	TableColumn tblclmnKolicina;
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public PregledIsporukaDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shellPregledIsporuka.open();
		shellPregledIsporuka.layout();
		Display display = getParent().getDisplay();
		while (!shellPregledIsporuka.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shellPregledIsporuka = new Shell(getParent(), getStyle());
		shellPregledIsporuka.setSize(564, 292);
		shellPregledIsporuka.setText(getText());
		
		tableViewer = new TableViewer(shellPregledIsporuka, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setBounds(37, 23, 370, 157);
		
		 TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		  tblclmnPolaznaLuka = tableViewerColumn.getColumn();
		tblclmnPolaznaLuka.setWidth(100);
		tblclmnPolaznaLuka.setText("Polazi\u0161te");
		
		TableViewerColumn tableViewerColumn_2 = new TableViewerColumn(tableViewer, SWT.NONE);
		 tblclmnOdrediste = tableViewerColumn_2.getColumn();
		tblclmnOdrediste.setWidth(100);
		tblclmnOdrediste.setText("Odrediste");
		
		final TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		tblclmnMaterijal = tableViewerColumn_1.getColumn();
		tblclmnMaterijal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				//___________________________
				
				
				 
				// System.out.println(table.getColumnCount());
				 //System.out.println( table.getColumnOrder());
			//	table.getColumn(3).g
			
				 
				// table.setSortDirection(0);
				// table.setSortColumn(tblclmnMaterijal);
			}
		});
		tblclmnMaterijal.setWidth(100);
		tblclmnMaterijal.setText("Materijal");
		
		TableViewerColumn tableViewerColumn_3 = new TableViewerColumn(tableViewer, SWT.NONE);
		 tblclmnKolicina = tableViewerColumn_3.getColumn();
		tblclmnKolicina.setWidth(62);
		tblclmnKolicina.setText("Kolicina");
		
		Button btnObrisi = new Button(shellPregledIsporuka, SWT.NONE);
		btnObrisi.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				int tekuciRedak = tableViewer.getTable().getSelectionIndex();
				if (tekuciRedak >=0)
					listaNarudzbi.remove(tekuciRedak);
				if (listaNarudzbi.size() == 0){
					// dodat kontrolu ako je prazna lista da ubaci redak
				}
				
			}
		});
		btnObrisi.setBounds(438, 70, 68, 23);
		btnObrisi.setText("Obri\u0161i");
		
		   Listener sortListener = new Listener() {
			      public void handleEvent(Event e) {
			        TableItem[] tablica = table.getItems();
			       
			        
			        Collator collator = Collator.getInstance(Locale.getDefault());
			        TableColumn stupac = (TableColumn) e.widget;
			        int index;
			       
			        if (stupac== tblclmnPolaznaLuka) {
			        	index=0;  
			        	table.setSortDirection(SWT.DOWN);
			        	table.setSortColumn(tblclmnPolaznaLuka);
			        }
			        else if(stupac== tblclmnOdrediste){
			        	index=1;
			        	table.setSortDirection(SWT.DOWN);
			        	table.setSortColumn(tblclmnOdrediste);
			        }
			        else if(stupac== tblclmnMaterijal) {
			        	index=2;
			        	table.setSortDirection(SWT.DOWN);
			        	table.setSortColumn(tblclmnMaterijal);
			        }
			        else {
			        	index=3;
			        	table.setSortDirection(SWT.DOWN);
			        	table.setSortColumn(tblclmnKolicina);
			        }
			        for (int i = 1; i < tablica.length; i++) {
				          String string1 =tablica[i].getText(index);
				     
				          for (int j = 0; j < i; j++) {
				 
				            String string2 = tablica[j].getText(index);
				            System.out.println(collator.compare(string1, string2));
				           //System.out.println("j "+j+value2);
				           if (collator.compare(string1, string2) < 0)
				           {
			
				        	   String[] noviPoredak = { tablica[i].getText(0), tablica[i].getText(1),tablica[i].getText(2),tablica[i].getText(3) }; 
				        	   tablica[i].dispose();
					           TableItem novaTablica = new TableItem(table, SWT.NONE, j);
					           novaTablica.setText(noviPoredak);
					           tablica = table.getItems(); 
					          // table.setSortColumn(tblclmnMaterijal);
					           
					              break;
				           }
				        
				            }
				          }
				        }
			       
			      };
			    
			      tblclmnPolaznaLuka.addListener(SWT.Selection, sortListener);
			      tblclmnOdrediste.addListener(SWT.Selection, sortListener);
			      tblclmnKolicina.addListener(SWT.Selection, sortListener);
			      tblclmnMaterijal.addListener(SWT.Selection, sortListener);
			     
		
		PostavkeCarobnjakaKontroler kontroler = new PostavkeCarobnjakaKontroler();
		listaNarudzbi=new WritableList(kontroler.ucitajNarudzbe(),PostavkeCarobnjakaKontroler.class);
		m_bindingContext = initDataBindings();
		
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		ObservableListContentProvider listContentProvider = new ObservableListContentProvider();
		tableViewer.setContentProvider(listContentProvider);
		//
		IObservableMap[] observeMaps = BeansObservables.observeMaps(listContentProvider.getKnownElements(), IsporukaModel.class, new String[]{"polaznaLuka", "odrediste", "materijal", "kolicina"});
		tableViewer.setLabelProvider(new ObservableMapLabelProvider(observeMaps));
		//
		tableViewer.setInput(listaNarudzbi);
		//
		return bindingContext;
	}
}
