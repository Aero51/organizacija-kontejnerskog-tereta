package OrganizacijaKontenjerskogTereta.controllers;

import OrganizacijaKontenjerskogTereta.models.IsporukaModel;
import OrganizacijaKontenjerskogTereta.view.NovaNarudzbenicaDialog;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.wizard.Wizard;


public class PostavkeCarobnjakaKontroler extends Wizard {
	String polazniText="Unesite polaznu luku:",polazniOpis="Unos polazišta";
    String odredisteText="Unesite odredište:",odredisteOpis="Unos destinacije";
    String vrstaTeretaText="Unesite vrstu tereta:",vrstaTeretaOpis="Vrsta tereta";
    List<IsporukaModel> listaNardudzbi=new ArrayList();
    String polaziste,odrediste,vrstaTereta,kolicina;
   
    
    NovaNarudzbenicaDialog polazisteDialog=new NovaNarudzbenicaDialog(polazniText, polazniOpis);
    NovaNarudzbenicaDialog odredisteDialog=new NovaNarudzbenicaDialog(odredisteText,odredisteOpis);
    NovaNarudzbenicaDialog vrstaTeretaDialog=new NovaNarudzbenicaDialog(vrstaTeretaText,vrstaTeretaOpis);
    @Override
    public void addPages() 
    {
        addPage(polazisteDialog);
        addPage(odredisteDialog);
        addPage(vrstaTeretaDialog);   
        
    }
    @Override
    public boolean performFinish() 
    {
    	
    	polaziste=polazisteDialog.getRezultat();
    	odrediste=odredisteDialog.getRezultat();
    	vrstaTereta=vrstaTeretaDialog.getRezultat();
    	kolicina=vrstaTeretaDialog.getKolicina();
    	
    	//isporukemodel.setPolaznaLuka(polaziste);
		//isporukemodel.setOdrediste(odrediste);
		//isporukemodel.setMaterijal(vrstaTereta);
		//isporukemodel.setKolicina(kolicina);
        // Handle finishing the Wizard here
    	// System.out.println("Ovo je odrediste"+odrediste);
    	 //System.out.println(polaziste.equals(odrediste));
    	 //System.out.println(polazisteDialog.canFlipToNextPage());
    	System.out.println("polaziste="+polaziste);
    	System.out.println("Odrediste="+odrediste);
    	System.out.println("Vrsta Tereta: "+vrstaTereta);
    	System.out.println("Kolicina="+kolicina);
    	
    	ucitajNarudzbe();
    	 spremiNarudzbu(polaziste, odrediste, vrstaTereta, kolicina);
    	
    		canFinish();
    	 return true;
    }
    
    
   

	public void spremiNarudzbu (String polaziste,String odrediste,String materijal,String kolicina) /*throws Exception*/ {
	
		 IsporukaModel isporukemodel = new IsporukaModel();
		{
			// IsporukaModel isporukemodel = new IsporukaModel();
			isporukemodel.setPolaznaLuka(polaziste);
			isporukemodel.setOdrediste(odrediste);
			isporukemodel.setMaterijal(materijal);
			isporukemodel.setKolicina(kolicina);
			
		listaNardudzbi.add(isporukemodel) ;
	
		//String pokazi;
		//System.out.println("Polazna luka model:"+isporukemodel.getPolaznaLuka());
		//System.out.println("Odre4diste:"+isporukemodel.getOdrediste());
		
		}
		try {
			FileOutputStream upisUDatoteku = new FileOutputStream("narudzbe.dat");
			ObjectOutputStream upisObjekta = new ObjectOutputStream(upisUDatoteku);
			upisObjekta.writeObject (listaNardudzbi);
		} catch (IOException e){
			e.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
			System.out.println("Nepoznata greška");
		}
	}




	public List<IsporukaModel> ucitajNarudzbe () {
		//List<SpremnikModel> listaLuka = new ArrayList<SpremnikModel>();
		try {
			FileInputStream citanjeDatoteke = new FileInputStream("narudzbe.dat");
			if (citanjeDatoteke.available() > 0) {
				ObjectInputStream citajObjekt = new ObjectInputStream (citanjeDatoteke);
				listaNardudzbi = (List<IsporukaModel>) citajObjekt.readObject();
				
				
			}
		} catch (ClassNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		return listaNardudzbi;
	}
    
    
}