package OrganizacijaKontenjerskogTereta.models;

public class PrijavaUsustavModel {

	private String sifra;
	private boolean status;
	private int stanje;

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getStanje() {
		return stanje;
	}

	public void setStanje(int stanje) {
		this.stanje = stanje;
	}
	
	
	
}
